
## Reviewing Read Level Support

In the Interpretation Browser users can click on the “Variant” hyperlink (chromosome:position) and view the read level 
support for their variant in the BAM through [IGV.js](TBC). 

The user is redirected to log in again with AD into the openCGA application which displays the selected alignment and 
variant tracks.

In addition, users can download a batch script from IGV.js that facilitates viewing the read level support in the 
desktop version of IGV.

For further details of how to use IGV in the Interpretation Browser, please refer to the existing 100,000 Genomes 
Project Genomics England Guide to Clinical Reporting for Rare Disease. 

## Visualising CNV breakpoints


--8<-- "includes/abbreviations.md"

