---
hide:
  - navigation
  - toc
---

???+ info inline end

    This documentation aims to consolidate the following user guides into a single site:
    
    1. [GMS Interpretation Portal guide](https://future.nhs.uk/NHSgenomics/view?objectId=108130629)
    2. [Interpretation Portal Documentation](http://ip-documentation.genomicsengland.co.uk/)
    3. 100K Cancer Portal guide (GUI BIO 001 Genomics England Guide to Interpretation Portal Cancer)
    4. Links to the exisiting 100K user guides
    
    In addition, it provides updated links to new improved documentation for other applications in the Interpretation 
    platform

The purpose of this site is to provide NHS clinical scientists, clinicians, bioinformaticians a step-by-step guide to 
the Genomics England Interpretation Portal.

This site covers how users Interpretation Portal can:

1. See an **overview of referrals** ready for NHS GLH review, and track overall referral status.
2. **Review findings** from Interpretation Services such as Tiering and Exomiser.
3. **Download any available files** and link out to Decision Support Systems.
4. Complete a **Reporting Outcomes questionnaire** to close a referral.
5. **Save work in progress** as a draft and return to it later e.g. when completing the Reporting outcomes questionnaire.
6. **Access 100,000 Genomes Project** Primary and Additional Findings.

## Getting Started      

[GMS Interpretation Portal](GMS-Interpretation-Portal/gms_get_started.md){ .md-button .md-button--primary } [100K Interpretation Portal](100k_background.md){ .md-button .md-button--primary }

## Additional Resources       

[ :link: ](https://genomicsengland.gitlab.io/interpretation/ip-docs-home/0.1/) Interpretation Platform Documentation 

[ :link: ](https://www.genomicsengland.co.uk/?wpdmdl=15664) 100K Rare Disease Analysis Guide 

[ :link: ](https://www.genomicsengland.co.uk/about-genomics-england/the-100000-genomes-project/information-for-gmc-staff/cancer-programme/) 100K Cancer Documentation 

[ :link: ](https://future.nhs.uk/NHSgenomics/view?objectId=84281317) Genomics England Rare Disease Genome Analysis Guide v2.0 


--8<-- "includes/abbreviations.md"