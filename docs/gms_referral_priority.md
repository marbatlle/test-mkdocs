Both Cancer and Rare Disease WGS Referrals in the GMS ordered through TOMS will have either a `Routine` or `Urgent` 
priority assigned to them when they are ordered.

The referral priority set in TOMS can be seen in the Interpretation Portal on the [Referral Grid](gms_referral_grid.md) 
and on the overview pages of [rare disease](gms_rd_referral_overview.md) and [cancer](gms_cancer_referral_overview.md) 
referral.

It is possible to sort referrals on the  [Referral Grid](gms_referral_grid.md) by their priority, however, it is not
possible to edit the priority of a referral in the Interpretation Portal

!!! Note

    The priority for cases in the 100K Project was `Low`, `Medium`,`High` or `Urgent`. This priority enumeration can 
    still be seen in any raw JSON data downloaded from the Portal or CIP-API. 
    
    For further information please refer to the CIP-API documentation [here](https://genomicsengland.gitlab.io/interpretation/cip-api-docs/response_definitions/#interpretation-request-list-api2interpretation-request)
    
--8<-- "includes/abbreviations.md"

