---
hide:
- toc
---

## Access the Interpretation Portal

The GMS Interpretation Portal can be accessed by users (on the HSCN network) here:

[Access the GMS Interpretation Portal](https://cipapi.genomicsengland.nhs.uk/interpretationportal/gms/){align=right .md-button .md-button--primary}

To login click “Login with NGIS AD Credentials”. You should use your NHS.net username and password or your smart card credentials which will have been set up by Genomics England Service Desk. 

??? note "Did you encounter any login issues?"

    First, ensure you are connected to HSCN.
    
    If problems persist, please try [logging out](https://cipapi.genomicsengland.nhs.uk/accounts/logout) and then attempt to access the Interpretation Portal again.
  
    In the event that problems still persist, please contact the [Genomics England service desk](http://bit.ly/ge-servicedesk).
    
    If you require user access, please follow local GLH procedures.

??? Attention "Did your session expire?"

    **Users will be logged out of the system after 20 minutes of inactivity. **
    
    If a user is logged out after 20 minutes inactivity, the page will prompt the user to log back in and return to the page the user was last on.
    
    A user's login token will be **refreshed** (and therefore the 20 minute timeout will be reset) with any of the **following actions**:
    - Opening a Rare Disease case
    - Opening a Cancer case
    - Opening the tiering browser in a Rare Disease case
    - Saving a draft summary of findings
    - Clicking the "Review Cases" button
    - Downloading a file
    
    Any other network request to the API will also refresh the token and reset the timer to 20 minutes.
    
    User activity in other NGIS services e.g. DSS currently does not refresh the users access token

## Find a Referral

The GMS Interpretation Portal has six tabs representing case statuses, with each tab containing a **Case Grid**, where users can find a specific referral through this interface. 

[Case Grid Guide](gms_case_grid.md){.md-button}


## Interpret your referral

Once you've successfully logged in, and you understand how to use the Case Grid, please refer to the following sections for further information on how to perform an interpretation for a case:

[Rare Disease Guide](gms_rd_quickstart.md){.md-button} [Cancer Guide](interp_gms_cancer_referral.md){.md-button}

--8<-- "includes/abbreviations.md"
