---
hide:
- toc
---


# Reviewing a GMS Rare disease referral

The GMS Interpretation Portal enables users to review a GMS Rare Disease Referral in NGIS using the DSS, as well as following a simplified workflow without using this service. In the following tabs, we offer an overview of these methods.

=== "Simple Workflow"

    ## Interpret your referral without DSS

     The following video provides an overview of the simplified workflow users can follow to review a GMS Rare Disease Referral, including the **selection of pertinent variants** and creation of the **Summary of Findings**. Find additional information  in the following tabs.

    ![type:video](content/IP-Guides-RDWorkflow-v0.2.mp4)

    ### The Steps
    
    1. Identify referral from the [referral grid](gms_get_started.md) "To Be Reviewed" tab.

    2. Navigate to the [Interpretation Browser](gms_rd_tiering_browser.md) page for that referral.

    3. Apply [filters](gms_rd_tiering_browser.md#TBC) (e.g. by Tier and / or Exomiser score) to review candidate variants

    4. Interpret candidate variants using local best practices (e.g. ACMG scoring) and review [read level support in IGV](gms_rd_tiering_browser.md#TBC)

    5. [Flag any pertinent variants](gms_rd_tiering_browser.md#TBC) to be included in the Summary of Findings

    6. Generate a [Summary of Findings](TBC) and include in local patient record if required

    7. Complete the [Reporting Outcomes Questionnaire](TBC)

=== "Using DSS"

    ## Interpret your referral with DSS

    As described [here](TBC) DSS provide more functionality (non-PASS variants, application of gene panels, ACMG scoring, granular approval workflows) that are not present in the Interpretation Portal.

    If you wish To interpret your referral, and create a Summary of Findings for it using the DSS you can link to it from the [Rare Disease referral overview page](gms_rd_referral_overview.md) 

    ???+ "Creating a Summary of Findings in the DSS"

        Creating an SoF in the DSS will result in the primary findings be sent from the DSS to the Interpretation Portal where they can be downloaded as a Summary of Findings HTML. The [Reporting Outcomes Questionnaire](TBC) for SoFs created in the DSS need to be completed in the Interpretation Portal to close the referral. 

=== "Generating a new Summary of Findings"

    At any time the Interpretation Browser can be used to create a new SoF for a referral (as described [above](#Simple-workflow-to-close-a-case-using-the-Interpretation-Portal))

    When a new SoF is created the referral will move from the "Reported cases" tab to the "Pending outcomes" tab of the referral grid

    ---
    Movement of a referral into the next stage – from “To Be Reviewed” into “Pending Outcomes” (and into the next tab of the
    Interpretation Portal) - requires a Summary of Findings (known as a “Clinical Report” in the CIPAPI) to be generated in 
    either the DSS or within the Interpretation Portal. 

    Without a Summary of Findings, the case will remain at “To Be Reviewed””. 

    When a Summary of Findings has been created, the referral will move to the “Pending Outcomes” tab of the Interpretation Portal.
    If a “Reporting Outcomes Questionnaire” is completed for a Summary of Findings, the referral will move to the “Reported Cases” tab in the Interpretation Portal. Unless a new Summary of Findings is generated for the referral, the referral will remain in the “Reported Cases” tab and in three months of residing in this state, the URL linking out to the DSS will be retired. This is considered the “Completed” state.
    If a new Summary of Findings is generated for the referral, the referral will move back into the “Pending Outcomes” tab until a new Reporting Outcomes  Questionnaire is completed. Note that when the report is downloaded, the “patient choice” is checked again.

--8<-- "includes/abbreviations.md"
