When on the ["Referral Grid"](gms_referral_grid.md) click the “Search by Personal Identifiable Data” button 

This opens a pop-up box where you can search for a particular case using either the NHS number & DOB (*1) or using DOB, 
First name, Last name and Gender.

![Image](img/gms_referral_search.png)

This will search for referrals that exist in the Interpretation Portal and filter the grid to show them

--8<-- "includes/abbreviations.md"
