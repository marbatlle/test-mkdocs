A Summary of Findings (SoF) aka Clinical Report represents the pertinent variants that have been identified as being
relevant to the proband's clinical indication

When a user selects a referral from the "To Be Reviewed" tab a [generates a SoF](interp_gms_rd_referral.md) either using 
the Interpretation Browser or DSS, the Referral moves to the "Pending Outcomes" tab on the [Referral Grid](gms_referral_grid.md).

A referral can have multiple SoFs generated.

To "close" a referral i.e. move it to the "Reported" tab, the Reporting Outcomes Questionnaire(TBC) must be completed 
for the most recent SoF

All Summary of Findings HTML files for a referral can be downloaded from the [Referral Overview](gms_rd_referral_overview.md)
page

## Generate a Summary of Findings using the Interpretation Browser

- Navigate to the [Interpretation Browser page](gms_rd_tiering_browser.md) for you referral.

- Filter / score variants according to your local best practice guidelines

- Select the report event for the variant(s) that you wish to include in the SoF, add a comment as to why you have 
  selected them e.g. ACMG criteria

!!! Attention

    Do not select >1 report event for a single variant

- Click the "Create Summary of Findings" button

- Review the variant level comments you added and add any additional case level comments in the "Interpretation" 
  box provided
  
- Please provide PubMed IDs of any articles used during the Interpretation.

!!! PubMed IDs

    Articles will be linked to the cases, variants and genes included in the SoF and made available through CVA

- Click "Submit"

![Image](img/gms_rd_sof_modal.jpg)

??? Key

    | #  | Section                    | Description                                                                                                                                                                                     |
    |:---|:---------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | SNVs / Indels              | List of the SNVs / Indel report events that have been selected as the Primary Findings for the case using the Interpretation Browser.                                                           |
    | 2  | Variant comments           | Add comments regarding the variant here. Note these comments get sent through to CVA so should not include any PID in this field                                                                |
    | 3  | CNVs & STRs                | If CNVs and STRs are selected as Primary Findings for the case they will be listed here.                                                                                                        |
    | 4  | Case comments              | Add overall interpretive comments here. Note these comments get sent through to CVA so should not include any PID in this field.                                                                |
    | 5  | Supporting evidence        | Add the PubMed IDs for articles relevant to the case and their primary findings here. Note, there is currently no validation on this field, however, please only include valid PubMed IDs here. |
    | 6  | Create Summary of Findings | Either “Save Draft” or “Submit” to create a SoF. The SoF will then be downloadable in HTML format and the case can be “closed” by completing the Outcomes Question.                             |
    
Upon generation of the SoF, the case will automatically move to the “Pending Outcomes” tab. 

The SoF HTML will be downloadable from the [“Rare Disease Case Overview Page”](gms_rd_referral_overview.md) page and 
the associated [Reporting Outcomes Questionnaire](TBC) can be completed.

If any errors are encountered generating the report, a message at the top of the report will appear alerting the user. 

If this error appears while generating a report, the NHS GLH user should report this to the Genomic England service desk 
(email ge-servicedesk@genomicsengland.co.uk  or via the portal www.bit.ly/ge-servicedesk), with the appropriate referral 
ID and a description of what happened, and a screenshot if possible.

## SoF HTML

![Image](img/gms_rd_sof_html.png)

??? Key

    | #  | Section                     | Description                                                                                       |
    |:---|:----------------------------|:--------------------------------------------------------------------------------------------------|
    | 1  | Patient ID                  | Patient ID for the proband in the Rare Disease Family.                                            |
    | 2  | Date                        | Date and time the Summary of Findings was generated.                                              |
    | 3  | Referral Information        | Details of the Referral Test.                                                                     |
    | 4  | Patient Information         | PID queried from TOMS.                                                                            |
    | 5  | Pedigree Table              | Lists details of the family members included in the referral and their consent.                   |
    | 6  | Gene Panels                 | Gene Panel Names and Versions used in the analysis                                                |
    | 7  | Details of Primary Findings | Details of the variant(s) selected as Primary Findings in the case                                |
    | 8  | Tier                        | If a tiered variant the tier will display here. If untiered it will display a “-”                 |
    | 9  | Allele Frequency            | Details of the variants frequency in external databases held in CellBase                          |
    | 10 | Patients                    | Details of the patients genotypes for the selected variant                                        |
    | 11 | Comments                    | Comments added to the variant during Interpretation. Please do not add PID here.                  |
    | 12 | Copy Number Variations      | Additional variant types selected as Primary Finding will also display here                       |
    | 13 | Report Context              | Details and limitations related to the specific version of the pipeline used to analyse this case |

--8<-- "includes/abbreviations.md"
