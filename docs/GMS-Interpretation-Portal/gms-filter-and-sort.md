The GMS Interpretation Portal offers this interactive interface, which enables the users to:

??? "Filter referrals by Sample Type"
    By default, the dispay shows all the cases, if you wish to only view **cancer** or **rare disease** cases, untick the respective boxes.

??? "Filter referrals by Your Cases"
        By default, the “View My Cases” checkbox above the case grid is selected. If you wish to view cases from an organisation outside of your GLH, uncheck this box.

??? "Sort referrals by Dates"
    From the column names, you can sort referrals by the date of the test was taken (**TOMS Order Date**), as well as by the date the referral has successfully arrived in the DSS (**DSS Arrival date**)

??? "Sort referrals by Status"   
    The possible statuses of the referral could be ...

??? "Sort referrals by Requesting Organization" 
    Requesting organisation defined during test ordering - what's the point?

??? "Sort referrals by Interpreter organisation" 
    Interpreter organisation defined during test ordering. - what's the point?     

??? "Sort referrals by Referral Priority" 
    Both Cancer and Rare Disease WGS Referrals in the GMS ordered through TOMS will have either a `Routine` or `Urgent` priority assigned to them when they are ordered.

    The referral priority set in TOMS can be seen in the Interpretation Portal on the [Referral Grid](gms_referral_grid.md)and on the overview pages of [rare disease](gms_rd_referral_overview.md) and [cancer](gms_cancer_referral_overview.md) referral.

    It is possible to sort referrals on the  [Referral Grid](gms_referral_grid.md) by their priority, however, it is not possible to edit the priority of a referral in the Interpretation Portal

    !!! Note

        The priority for cases in the 100K Project was `Low`, `Medium`,`High` or `Urgent`. This priority enumeration can 
        still be seen in any raw JSON data downloaded from the Portal or CIP-API. 
        
        For further information please refer to the CIP-API documentation [here](https://genomicsengland.gitlab.io/interpretation/cip-api-docs/response_definitions/#interpretation-request-list-api2interpretation-request) 

??? "Filter by Patient choice" 
    Green: All sequenced patients have a positive primary genomic testing consent.<br>Amber: Consent status of least one sequenced individual is “n/a” and no sequenced individual has ambiguous or negative consent.<br>Red: At least one sequenced individual has ambiguous or negative consent.

^^Add screenshot and Figure Description^^