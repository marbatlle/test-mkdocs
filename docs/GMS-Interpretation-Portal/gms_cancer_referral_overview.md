The Cancer Referral Overview page highlights the patient information, the number of domain 1/2/3 somatic variants, and 
germline variants interpreted by the Genomics England Cancer Interpretation Pipeline.

![Image](img/gms_cancer_overview.jpg)

??? Key

    | #  | Section                   | Description                                                                                                                                                                                                                                                                                                                                           |
    |:---|:--------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | Patient Summary           | The Referral ID for the patient.                                                                                                                                                                                                                                                                                                                      |
    | 2  | DSS                       | For example, Congenica for rare disease referrals or Illumina BaseSpace Variant Interpreter (BSVI) for cancer referrals.                                                                                                                                                                                                                              |
    | 3  | Referral Priority         | Priority for the case, e.g. “Routine” or “Urgent”.                                                                                                                                                                                                                                                                                                    |
    | 4  | Interpretation Services   | List of the interpretation services that have been applied to the case.                                                                                                                                                                                                                                                                               |
    | 5  | Interpreting Organisation | Defined during test ordering.                                                                                                                                                                                                                                                                                                                         |
    | 6  | Case Status               | Interpretation Status of the case, e.g. whether or not it is has a SoF ready.                                                                                                                                                                                                                                                                         |
    | 7  | Requesting Organisation   | Defined during test ordering.                                                                                                                                                                                                                                                                                                                         |
    | 8  | Workflow status           | The workflow status for the referral.                                                                                                                                                                                                                                                                                                                 |
    | 9  | Sex                       | The sex of the patient.                                                                                                                                                                                                                                                                                                                               |
    | 10 | Notes                     | Notes added to the case during Test Order.                                                                                                                                                                                                                                                                                                            |
    | 11 | Patient                   | Link out to TOMS. If available, PID will be queried from TOMS and displayed here.                                                                                                                                                                                                                                                                     |
    | 12 | Patient Choice            | Yes- patient has agreed to the test, No- patient has declined the test, NA- patient choice not relevant.                                                                                                                                                                                                                                              |
    | 13 | Tumour Local ID           | Information about the Tumour types; primary or metastatic; presentation and other details about the tumour.                                                                                                                                                                                                                                           |
    | 14 | Tumour Content            | Tumour content is displayed as High, Medium or Low with an accompanying tumour content percentage. The thresholds for High, Medium and Low are as follows: Low is <40% tumour content. Medium is 40-60%. High is >60%. Please note that the tumour content displayed here in the Portal is the content entered by the GLH at the point of test order. |

## Variant Summary

From the Cancer Referral Overview page you can view a summary of the tiered somatic and germline variants have been 
identified by the Cancer tiering pipeline. 

!!! Note

    Unlike for Rare Disease referrals users cannot view the tiered variants in Cancer referrals in the Interpretation Portal 
    
    All Cancer variant information, and links to IGV, are found in the Cancer Summary of Findings HTML and the 
    Supplementary Findings HTML. 
    
![Image](img/gms_cancer_var_summary.jpg)

## Cancer DSS

In addition to the [Primary](TBC) and [Supplementary](TBC) HTML files, from the Cancer Referral Overview page users can 
link to the Decision Support System (DSS) to review variants. 

A new Summary of Findings can be generated by the DSS and sent to the Interpretation Portal should users wish to do this.

## Feedback forms and File Downloads

From the Cancer Referral Overview page users can access the Cancer [Referral Feedback form](TBC)

Any files associated with the referral can be downloaded from here, including the [Primary](TBC) and [Supplementary](TBC) 
HTML files

![Image](img/gms_cancer_file_downloads.png)

For additional technical details of the Cancer Reports please refer to Technical Documentation available for download 
[here](https://www.genomicsengland.co.uk/about-genomics-england/the-100000-genomes-project/information-for-gmc-staff/cancer-programme/cancer-genome-analysis/)


--8<-- "includes/abbreviations.md"
