The Interpretation Browser can be accessed from the [Referral Overview Page](gms_get_started.md) and enables users to ["close" GMS Rare disease referrals]( gms_rd_quickstart.md) without using the DSS.

![Image](content/gms_rd_tiering_browser.png)

??? Key

    | #  | Section                                         | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
    |:---|:------------------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | Create Summary of Findings                      | Create a Summary of Findings for the case with the number of selected variants in the ().                                                                                                                                                                                                                                                                                                                                                                       |
    | 2  | SNV/Indels                                      | Tab displays the SNV and Indel variants prioritised by Interpretation Services applied to the case.                                                                                                                                                                                                                                                                                                                                                             |
    | 3  | Short Tandem Repeats                            | Tab displays the short tandem repeats found in the case.                                                                                                                                                                                                                                                                                                                                                                                                        |
    | 4  | Copy Number Variations                          | Tab displays the copy number variants found in the case.                                                                                                                                                                                                                                                                                                                                                                                                        |
    | 5  | VCF Files                                       | Tab lists the VCF files for each member of a family in a case, available for download.                                                                                                                                                                                                                                                                                                                                                                          |
    | 6  | Filters available in the Interpretation Browser | Filter variants by several parameters including by Tier (1,2 or 3), Panel Name, Penetrance, Disorder, Consequence type, Compound heterozygous and Exomiser score. Users can also enter specific gene names into the “Add variants from VCF” to add these to the bottom of the table. There is also an option to download variants as a TSV file and another option to select and add other variants from the vcf that are not prioritised in the variant table. |
    | 7  | Variant                                         | Coordinates and nucleotide change of a particular variant within a gene.                                                                                                                                                                                                                                                                                                                                                                                        |
    | 8  | Genes                                           | HGNC gene symbol and Ensembl gene ID in which the variant was found.                                                                                                                                                                                                                                                                                                                                                                                            |
    | 9  | Zygosity                                        | Zygosity of each member of a family for that particular variant, two unfilled circles indicating homozygous wild type, one filled and one unfilled circle indicating a heterozygote , two filled circles indicating alternate homozygous or a "-" indicating the genotype is undetermined.                                                                                                                                                                      |
    | 10 | Max Allele Freq                                 | Maximum alternate allele frequency for this position – from CellBase.                                                                                                                                                                                                                                                                                                                                                                                           |
    | 11 | Interpretation Service                          | Interpretation service that identified this variant.                                                                                                                                                                                                                                                                                                                                                                                                            |
    | 12 | Existing Classifications                        | Existing classifications for this variant in at least one other case in CVA.                                                                                                                                                                                                                                                                                                                                                                                    |
    | 13 | The ‘+’ sign on a variant                       | View additional information (including HGVS information) for a variant.                                                                                                                                                                                                                                                                                                                                                                                         |

Functionality includes:

- Review the variants, including Small Variants (i.e. SNVs & Indels), Copy Number Variants (CNVs) & Short Tandem Repeats
  (STRs) identified by interpretation services (Tiering and Exomiser) and their associated report events.
- Review the zygosity of each variant in all sequenced family members. 
  A filled black circle indicates presence of the variant, with a single black circle indicating heterozygosity and two 
  black circles indicating homozygosity for a variant. A "-" indicates the genotype is undetermined.
- See additional annotations (e.g. allele frequencies, HGVS annotations, in silico predictions) from CellBase for 
  variants.
- Report on additional variants from the VCF files associated with the case.
- Comment on and add interpretative comments
- Download selected tiered variants in a TSV (Tab Separated Values) file.
- Review gene panel coverage.
- Review read level support for variants using IGV.js.
- Generate a Summary of Findings.
- Fill out a reporting outcomes questionnaire.


### Download Variants

It is possible to download variants shown in the Interpretation Browser in a TSV format. By default, all variants are 
selected for download, however, it is possible to select one or more individual variants for download.

Variants from the entire genome, in VCF format, can be downloaded for all family members using the VCF files tab of the 
Interpretation Browser. For more information see [here](gms_rd_file_downloads.md).

### Add small variants from genes outside of the applied panels

This feature enables users to add variants from genes that were not included in the original panel used for variant 
prioritisation. 

This feature enables GLHs to assess variants in genes that might have only recently been identified as having an 
association with the clinical indication in the referral.

To add variants in a gene outside of the panel applied click the “Add variants from VCF” button. Enter the HGNC gene 
symbol for the gene you would like to review variants in and click the “Search” button.

The search term is case sensitive so “KCNJ11” will work whereas “kcnj11” will not.

After clicking the search button, any variants with a PASS status in the VCF files will be added to the Interpretation 
Browser and can be reviewed and reported in the same way. 

Please note that a phenotype must be assigned before the variant can be added to the summary of findings.

!!! Attention

    Only variants with a PASS status in the VCF file can be added to the Interpretation Browser table.
    Currently non-PASS variants can only be viewed in the [Congenica DSS](TBC)



## Short Tandem Repeats (STRs)

Results of [STR tiering](https://future.nhs.uk/NHSgenomics/view?objectId=84281317) can be viewed and STR pileup graphs 
downloaded. 

In addition, STRs can be selected and commented on for inclusion in the case’s Summary of Findings.

![Image](img/gms_rd_strs.png)

??? Key

    | #  | Section                | Description                                                                                                                                      |
    |:---|:-----------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | Short Tandem Repeats   | Tab displays the STRs in a case (if any)                                                                                                         |
    | 2  | Download STRs TSV      | Download the STRs in the case                                                                                                                    |
    | 3  | FMR1 notice            | In line with clinical advice, Genomics England do not analyse FMR1 repeat expansions. This message will appear on all cases with STRs            |
    | 4  | STR box                | This box contains the details of the STR, including the tier, locus, repeat motif, normal number of repeats and the pathogenic number of repeats |
    | 5  | Location               | The genomic coordinates for the STR                                                                                                              |
    | 6  | Panels                 | The panels that have been applied to the case                                                                                                    |
    | 7  | Zygosity               | This table details the number of copies the patient has on both alleles                                                                          |
    | 8  | Pile-Up Graph          | This button allows users to download the pile up graph for this STR                                                                              |
    | 9  | Report Events          | This table displays the details of the STRs found in the case                                                                                    |
    | 10 | Score                  | Displays the tier (if any) of the STR                                                                                                            |
    | 11 | Genomic Entity         | Displays the gene name and ENSG ID                                                                                                               |
    | 12 | Mode of Inheritance    | The mode of inheritance, as listed in PanelApp, for the gene on the panel applied                                                                |
    | 13 | Panel Name             | The panel(s) applied to the case in relation to the gene in the “Genomic Entity” column                                                          |
    | 14 | Panel Version          | The version of the panel applied                                                                                                                 |
    | 15 | Penetrance             | The penetrance of the clinical indication                                                                                                        |
    | 16 | Clinical Indication    | The clinical indication the patient has been assigned                                                                                            |
    | 17 | Interpretation Service | The interpretation service (e.g. Tiering) that has been applied to the variant to give the score                                                 |

## Copy Number Variants (CNVs)

!!! Attention

    Following the "Danny" release of NGIS, high quality calls between 2 kb-10Kb derived from the proband are additionally included in 
    tiered CNVs. These are CNVs that overlap with genes or contain regions defined in the virtual gene panel(s) applied 
    to the patient. High quality CNV calls >2 kb that neither overlap with genes nor contain regions defined in the 
    virtual gene panel(s) will be included in tier null CNVs.
    
    The CNV vcf for the proband, available for download in the interpretation portal, will include these cnvs. The CNV v
    cf extension is updated to .enhanced.cnv.vcf.gz where these CNVs are called.
    
    Please note that detection of CNVs between 2 kb and 10 kb is not currently included in the ISO15189 schedule of 
    accreditation issued by UKAS. Accreditation is anticipated in Q3 2021. Please refer to details of accreditation 
    [here](https://www.ukas.com/wp-content/uploads/schedule_uploads/00007/10170Medical-Single.pdf)


[CNV tiering](https://future.nhs.uk/NHSgenomics/view?objectId=84281317) results are displayed in two tables, the “CNV 
Call Level Table” and the “CNV Gene Level Table”. 

### CNV Call Level Table

The Call Level Table lists all the CNVs identified in the proband and allows users to select and comment on CNV calls 
they may want to include in Summary of Findings. 

![Image](img/gms_rd_cnvs.png)

??? Key

    | #  | Section                                                           | Description                                                                                                                                                                                                                               |
    |:---|:------------------------------------------------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | Copy Number Variations                                            | This tab displays the CNVs in the case (if any)                                                                                                                                                                                           |
    | 2  | Download CNVs                                                     | This button allows users to download all, or a selection of CNVs in a TSV file. Users can select particular CNVs to download by clicking on the “+” next to the genomic coordinates of the particular CNV and checking the box next to it |
    | 3  | Genomic Coordinates                                               | The genomic coordinates of the CNV (which link out to IGV) and the link to DGV                                                                                                                                                            |
    | 4  | Cytobands                                                         | The cytobands relating to the genomic coordinates of the CNV                                                                                                                                                                              |
    | 5  | CNV Type                                                          | Whether the CNV is a ’LOSS’ or a ‘GAIN’                                                                                                                                                                                                   |
    | 6  | Copy Number                                                       | The number of copies of the region present in the sample                                                                                                                                                                                  |
    | 7  | Size bp                                                           | The size in base pairs of the CNV                                                                                                                                                                                                         |
    | 8  | Allele Frequency by reciprocal overlap (LOSS only)                | The allele frequency of the CNV by the computational overlap between structural variants                                                                                                                                                  |
    | 9  | Allele frequency by frequency track (LOSS only)                   | The allele frequency from the frequency track applied                                                                                                                                                                                     |
    | 10 | Proportion of patients with CNV by reciprocal overlap (GAIN only) | The proportion (percentage) of patients with this CNV by overlap                                                                                                                                                                          |
    | 11 | Proportion of patients with CNV by frequency track (GAIN only)    | The proportion (percentage) of patients with this CNV by the frequency track applied                                                                                                                                                      |
    | 12 | Number of protein coding genes                                    | The number of protein coding genes affected by the CNV                                                                                                                                                                                    |
    | 13 | ISCA id of the pathogenic region in the panel applied             | The ISCA ID of the pathogenic region in the applied panel                                                                                                                                                                                 |
    | 14 | Link to ISCA ID in PanelApp                                       | The link out to the ISCA ID with pathogenicity                                                                                                                                                                                            |
    
 
### CNV Gene Level Table

The Gene Level Table lists all of the genes that overlap CNV calls in the proband.

![Image](img/gms_rd_cnv_genes.jpg)

??? Key

    | #  | Section                         | Description                                                                 |
    |:---|:--------------------------------|:----------------------------------------------------------------------------|
    | 1  | Gene                            | The gene name and gene ENSG ids affected by a CNV                           |
    | 2  | Genomic Coordinates of the CNV  | The genomic coordinates of the CNV                                          |
    | 3  | CNV Type                        | The CNV type (’LOSS’ or ‘GAIN’)                                             |
    | 4  | Copy Number                     | The number of copies in the patient sample                                  |
    | 5  | Size, bp                        | The size of the CNV                                                         |
    | 6  | All panels containing this gene | Genes which link to PanelApp, displaying the panels which include this gene |
    | 7  | Tiered                          | The tier of the CNV (TIERA or null)                                         |
    | 8  | DGV                             | A link out to DGV for this CNV                                              |
    
## Report Events

Variants (SNVs, Indels, CNVs and STRs) in the Interpretation Browser have one or more report events. 

Report events indicate how the variant is scored and weighed against the clinical indications depending on the panel 
and interpretation service applied. 

![Image](img/gms_rd_report_events.jpg)

??? Key

    | #  | Section                   | Description                                                                                                                                    |
    |:---|:--------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | Report Events             | Report events table. This can be viewed by clicking the ‘+’ sign to the left of the variant.                                                   |
    | 2  | Selection box             | The check box to the left of every report event enables the user to select it for reporting.                                                   |
    | 3  | Score                     | Priority assigned to the variant by an interpretation service against the clinical indication. This can change depending on the panel applied. |
    | 4  | Genomic entity            | Gene and the Ensembl gene id linking out to the ensemble gene page.                                                                            |
    | 5  | Mode of Inheritance       | Known mode of inheritance for the gene with respect to the clinical indication.                                                                |
    | 6  | Panel Name                | Each panel applied to the case is listed and used to prioritise.                                                                               |
    | 7  | Panel Version             | Panel version when the panel was applied to a particular case.                                                                                 |
    | 8  | Penetrance                | Known penetrance data of a gene with respect to the clinical indication.                                                                       |
    | 9  | Clinical Indication       | Clinical indication of the patient being assessed.                                                                                             |
    | 10 | Interpretation Service    | The interpretation service that assigned a priority to a report event.                                                                         |
    | 11 | Show/Hide Variant Details | This button helps the user to view/hide further information about a variant. See: Figure 9                                                     |

## Additional Annotations

Further information about a variant such as allele frequencies, HGVS annotations, *in silico* predictions and disease 
associations are retrieved from external databases held in CellBase. 

![Image](img/gms_rd_cellbase_anno.jpg)

??? Key

    | #  | Section                        | Description                                                                                                                                  |
    |:---|:-------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | Positional and allelic details | Variant coordinates, alleles and HGVS notation.                                                                                              |
    | 2  | Most severe consequence type   | Most severe consequence of the variant with respect to a transcript.                                                                         |
    | 3  | Consequence Types              | Other known or predicted consequences of the variant with respect to other transcripts of the gene and protein consequences when applicable. |
    | 4  | Population Frequencies         | Reported frequency of the variant in a number of populations is retrieved when available.                                                    |
    | 5  | Variant Trait Association      | Known association of the variant with respect to disease.                                                                                    |
    | 6  | Gene Trait Association         | Known associations of the gene where the variant is located with respect to disease.                                                         |


## Reviewing Compound Heterozygous Variants

TBC

--8<-- "includes/abbreviations.md"

