![Image](img/gms_portal_sms_checks.png)

??? "Patient Choice Checks"

    Before a rare disease referral is registered in the CIP-API, the [“patient choice”](gms_portal_patient_choice.md) (consent) 
    is checked for each patient in that referral. 
    
    If when the referral first arrives in the Portal the patient choice for any patient is not available, the referral will 
    be automatically blocked and will not be visible to users. 
    
    The referral will not be auto-dispatched to Congenica unless the patient consent is rescinded after dispatch.

When the referral arrives in the Interpretation Portal, it undergoes a [Sample Matching Service (SMS)](TBC) check. 

This can have 3 different outcomes:

1. **Pass SMS checks**
   Referrals in which each sample passes the SNP comparision are deposited into the “Ready to Dispatch” tab on the 
   Interpretation Portal. The referral will be in the queue to be automatically dispatched to the DSS.

2. **Pending SMS checks**
   Samples in Referrals can be missing the associated SNP VCF as it has not yet been uploaded by the GLH. In this 
   scenario the SNP check is classed as “pending” and is deposited into the “Identity Check Pending” tab.

3. **Fail SMS checks**
    Referrals that fail the SNP check are deposited into the “Identity Check Failures” tab. 
    They will be flagged with a “cross” next to the sample LP number on the referral page. A referral is this tab will 
    not be dispatched to a DSS until the identify check issue is resolved e.g. a new SNP VCF is uploaded by the GLH.
    Another SMS check will be automatic if a new SNP VCF has been uploaded in the case where there has been a failed SMS
    check previously. A successful SMS check is indicated by a tick icon next to the sample ID. 
    If SMS checks pass for all samples in the referral, it should then proceed to the “Ready to Dispatch” tab and be in 
    the queue to be automatically dispatched to the DSS.
    
!!! note 
    
    It is still possible to create a [Summary of Findings](tbc) from a referral if it is in either the 
    “Identity Check Failures” or “Identity Check Pending” tabs using the Interpretation Browser. 
    
    Creating a Summary of Findings from a referral which fails SMS checks is strongly advised against.

Upon successful loading of the referral in the DSS it moves to the “To Be Reviewed” tab and the DSS link from the case 
page is activated. Please see [How To Interpret Rare Disease referral](interp_gms_rd_referral.md) for next steps.

If the referral fails to load in the DSS it will remain in the “Ready to Dispatch” tab. 

Please inform the Genomics England Service Desk if you believe a case has failed to load in the DSS.

--8<-- "includes/abbreviations.md"
