## Rare Disease Referral Overview Page 

To open a Referral and see an overview – click on the Referral ID in the [referral grid](gms_referral_grid.md) for the case you wish to review, 

![Image](img/gms_rd_referral_overview.png)

??? Key
    
    | #  | Section                   | Description                                                                                                                                                                                            |
    |:---|:--------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | 1  | DSS                       | View the referral in the DSS.                                                                                                                                                                          |
    | 2  | Interpretation Browser    | Browse results from Interpretation Services.                                                                                                                                                           |
    | 3  | Gene panels Coverage      | View gene panel coverage for each panel applied to the referral.                                                                                                                                       |
    | 4  | CVA                       | Link out to CVA to compare cases and variants.                                                                                                                                                         |
    | 5  | Payload                   | Download the payload for the referral.                                                                                                                                                                 |
    | 6  | Referral Priority         | Referral priority set as “Urgent" or “Routine" during test ordering.                                                                                                                                   |
    | 7  | Case Status               | Interpretation Status of the case, e.g. whether or not it is has a clinical report ready (see: GMS Interpretation Portal Tabs).                                                                        |
    | 8  | Workflow status           | Represents the workflow status of the referral. See: [Rare Disease Case Overview Page](#scroll-bookmark-80).                                                                                           |
    | 9  | Interpretation Services   | List of the interpretation services that have been applied to the referral.                                                                                                                            |
    | 10 | Genome Assembly           | Displays which assembly the referral is on (GRCh38).                                                                                                                                                   |
    | 11 | Interpreting organisation | Defined during test ordering.                                                                                                                                                                          |
    | 12 | Requesting organisation   | Defined during test ordering.                                                                                                                                                                          |
    | 13 | Notes                     | Notes added to case during test ordering.                                                                                                                                                              |
    | 14 | Clinical Indication       | From the Test Directory defined during the test order.                                                                                                                                                 |
    | 15 | Penetrance                | Penetrance model that the referral is being analysed under i.e. complete or incomplete.                                                                                                                |
    | 16 | Affiliated Panels         | The gene panels that have been applied to the referral. For GMS cases the panel name is a hyperlink to the [GMS signed off panel resource](https://nhsgms-panelapp.genomicsengland.co.uk/) of PanelApp |
    | 17 | Case metadata table       | Displays the patient ID, the NHS number, the patients' full name, their date of birth, sex, relationship to the proband, disease status (affected/unaffected).                                         |
    | 18 | Present HPO terms         | HPO terms for that individual within the referral. HPO terms link out to the HPO database.                                                                                                             |
    | 19 | Sample ID and SMS result  | Sample IDs and results of SMS check (see: [Sample Matching Service](#scroll-bookmark-81)).                                                                                                             |
    | 20 | Patient Choice            | Answers to patient choice question (see: Patient Choice (Consent) Overview).                                                                                                                           |

In this section users can:

- View the pedigree associated with the referral.
- See the “Interpretation Services” applied to a case.
- Link to the [Rare Disease Interpretation Browser](TBC).
- Link out to [Decision Support Systems (DSS)](TBC).
- Link out to [Clinical Variant Ark (CVA)](TBC).
- Download the associated Interpretation Request Data in JSON format.

??? Display of PID

    The pedigree table includes the patients’ NHS numbers, their names, their dates of birth and their consent status, which
    are queried directly from TOMS. 
    
    If there is any issue accessing this data from TOMS the pedigree table will still be displayed, however, the PID data 
    will be displayed as “N/A”.

Below the header section there is a summary of the Interpretation Service results applied to the referral and a list of 
the [associated files](gms_rd_file_downloads.md) available for download. 

From here users can download [Summary of Findings](TBC) and complete [Reporting Outcomes Questionnaires](TBC)

??? Referral Statuses
    
    For details of the overall referral status see: https://cipapi-documentation.genomicsengland.co.uk/usage/#api-statuses


--8<-- "includes/abbreviations.md"
