Collecting reporting outcomes through the Interpretation Portal is important in informing the knowledge base and allows 
the constant improvement of the pipeline. 

The outcomes inputted are automatically transferred into the CVA knowledge base which is available for all GLH users to
access.

When a case has been reviewed in the Interpretation Browser (or DSS) and a SoF generated (Please see the individual DSS 
user guides for specific instructions) the case will move into the "Pending Outcomes" tab.

To complete the reporting outcomes questionnaire and close a rare disease referral, the user should click on the 
“Review” button next to the “Download” button in the SoF table.

1. The reporting outcomes questionnaire will then be available to populate from a list of drop-down menus.
2. The variants displayed in the questionnaire are those contained within the associated version of the SoF. If several versions of a SoF are generated, it is possible to complete a Reporting outcomes questionnaire for each one. It is only necessary to complete one questionnaire per case, usually this would be against the most recent version of the SoF: if a reporting outcomes questionnaire is completed for a case but then another SoF is generated, the case will move from closed (“Reported”) to “Pending Outcomes”.
3. The reporting outcomes questionnaire is divided into family level and variant level questions. 

For “negative” reports containing no variants, the questionnaire will only present the family level questions.

Once completed, please click on the “submit” button at the bottom of the page to close the case and move it into the 
"Reported Cases" tab.

If you need to revise your report, please open it again as [described](interp_gms_rd_referral.md), complete it again, 
and resubmit. This version will over-ride the previous version as only one questionnaire can be stored against each SoF.

An RoQ can be saved as a draft and returned to be completed at another time. 

## Family Level Questions

???+ question "Have the results reported here explained the genetic basis of the family’s presenting phenotype(s)?"
    
    Do the combined variants between them explain the genetic basis of the presenting phenotype(s) in the referral. 
    This is asking whether the case can be considered fully or partially solved?

???+ question "Have you done any segregation testing in non-participating family members?"

    If you have, please enter details in the free text comments box below. 
    
    Please do NOT include any identifying details in this box. 
    
    For example, “Proband’s maternal uncle also affected with ataxia and carries the variant in gene X” is acceptable; 
    
    “Also tested Robert Smith for the variant in gene X” is not acceptable.

## Variant Level Questions

???+ question "Did you carry out confirmation of this variant via an alternative test?<br>Did the test confirm that the variant is present?<br>Did you include the variant in your report to the clinician?<br>What ACMG pathogenicity score (1-5) did you assign to this variant?<br>Please provide PMIDs for papers which you have used to inform your assessment for this variant"

    Five questions are asked about each individual reported variant. It is not mandatory to enter details of publications used to support the reporting outcome, but if you have used any in the clinical report, please enter them here, they will be added to CVA.

???+ question "Is evidence for this variant/variant pair sufficient to use it for clinical purposes such as prenatal diagnosis or predictive testing?"

    TBC

???+ question "Has the clinical team identified any changes to clinical care which could potentially arise as a result of this variant/variant pair?"
    
    TBC

???+ question "Did you report this variant/variant pair as being partially or completely causative of the family's presenting phenotype(s)?"
    
    TBC

Three questions are asked at the level of a variant or pair of compound heterozygous variants. The first question asks whether you have reported the variant or variant pair as having sufficient supporting evidence to be used for clinical purposes such as prenatal diagnosis or cascade/predictive testing.
The second question asks about any potential clinical outcomes which may have been identified during discussion with the clinical team, e.g. at the results MDT meeting. If this information is not available, please record it as unknown.
The third question asks if you have included in the clinical report, the particular variant or variant pair in question as directly causative, either partially or completely, to the presenting phenotypes in the referral.

--8<-- "includes/abbreviations.md"


