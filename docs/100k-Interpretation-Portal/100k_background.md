The 100K Interpretation Portal is the pre-cursor to the GMS Interpretation Portal

It has much the same functionality as the GMS Interpretation Portal.

[Access the 100k Interpretation Portal](https://cipapi.genomicsengland.nhs.uk/interpretationportal/100k){: .md-button .md-button--primary}

Unlike the GMS, access to results from the 100K Project are restricted to the GMCs involved with the recuitment of the 
participants.

If you require access to the 100K Interpretation Portal please contact <ge-servicedesk@genomcisengland.co.uk>

## 100K Interpetation Portal Rare Disease User Guide
The 100K Interpretation Portal user guide can be found in Section 5.13 of the 100K Rare Disease Results Guide [here](https://www.genomicsengland.co.uk/?wpdmdl=15664)

## 100K Interpretation Portal Cancer User Guide

100K Cancer Documentation can be found [here](https://www.genomicsengland.co.uk/about-genomics-england/the-100000-genomes-project/information-for-gmc-staff/cancer-programme/) 
