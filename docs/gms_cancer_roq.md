On the [Cancer Referral Overview Page](gms_cancer_referral_overview.md), under Feedback Forms, you can complete the RoQ 
for an SoF by clicking the `Review` button.

!!! info
    
    Only the last version of the RoQ, for each SoF version will be stored. 
    
    If a Cancer referral has multiple SoFs, it is possible to have multiple feedback forms, e.g. due to the review of a 
    patient as the disease progresses.

Under “Feedback Forms”, you can update and/or edit any Cancer RoQ by clicking on the `Edit` which will bring you to the 
Cancer Referral Feedback Form page.


There will be a feedback form for each SoF. 

Any previous answers to a particular SoF version can be reviewed by clicking on “Previous Answers” at the top of the 
Feedback Form.

!!! note 
    
    any field with a red asterisk * is a mandatory field that must be completed before the Feedback Form can be 
    successfully saved. 

Case Level Questions include the Analysis, Validation, and Reporting time for the case, which are reported in hours 
(e.g. 1, 2, 2.5, etc.).

In the Case Level Questions, the WGA (Whole Genome Analysis) Outcomes include the option for a user to select either 
that Domain 1, 1 and 2, or 1 and 2 and supplementary analysis were reviewed for that particular case.

Additional Comments is a free text field that will not be curated in any programmatic way.  Therefore, add any enriched information that cannot be entered in either the Case Level Questions or the Variant Level Questions.

## Case Level Questions

???+ question "Analysis, validation and reporting time"
    
    **Review Time**
    
    - Total time taken to review/collate evidence for variants (hours). Include all literature review time, consultation with relevant experts etc.<br>
    - Time taken to discuss case at MDT (hours).<br>
    - If the case is discussed at a 2nd MDT please enter time here (hours).<br>
    
    **Validation Time**
    
    - Total time to design ALL validation assay(s) for case (hours). Only applicable if it is necessary to design a new assay to validate the variant.
    - Technical Laboratory Validation. Total time for validation test wet work for all variants (hours).
    - Analytical Laboratory Validation. Total time for analysis of validation results for all variants (hours).
    
    **Reporting Time**
    
    - Primary Reporting. Time taken to complete primary reporting stage (hours).
    - Report Authorisation. Time taken to check and authorise report (hours).
    - Report Distribution. Please enter, where possible/accessible how long it takes for the result to be conveyed to the patient. E.g. via letter from the clinician (days).
    - Total time from result to report. The total time taken from when the analysis of the WGS results started to a report being received by the patient include any "waiting" time (days).
    
    **WGA Outcomes**    
    
    - Which parts of the WGA were reviewed?
    - Are there any variants in this report that would be potentially actionable for the tumour of this patient?
    
???+ question "Additional comments"

    Free text field for entering any additional comments about the referral

## Variant questions

???+ question "Variant Level Questions - Potentially Actionable Variants"

    Questions to be answered for the potentially actionable variants in the referral
    
    - Type of (potential) actionability:
        - Predicts therapeutic response?
        - Prognostic?
        - Defines diagnosis group?
        - Eligibility for trial?
        - Other?
    - How has/will this potentially actionable variant or entity been/be used?
    - Has this variant or entity been tested by another method (either prior to or following receipt of this WGA)?


### Pathogenic germline cancer susceptibility variants

TBC

### Other actionable variants or entities. 

This enables the user to add or remove user-edited and defined variants, mutational signatures, mutational burden, or any other entity

- Somatic Domain 3 small variant
- Germline Tier 3 small variant
- SV CNV
- Mutational Signatures
- Mutation Burden
- Other

![Image](img/gms_cancer_roq_other_vars.png)

