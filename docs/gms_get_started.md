Access the GMS Interpretation Portal here: https://cipapi.genomicsengland.nhs.uk/interpretationportal/gms/

!!! note 
    
    You must be on the HSCN (previously the N3) network.

Click “Login with NGIS AD Credentials” – these will be your NHS.net username and password which will have been set up by
Genomics England Service Desk.

If you encounter login issues, please ensure you are connected to HSCN. 

If problems persist, please try logging out by going to https://cipapi.genomicsengland.nhs.uk/accounts/logout and then 
attempt to access the Interpretation Portal again.

In the event that problems still persist, please contact the [Genomics England service desk](http://bit.ly/ge-servicedesk).

If you require user access, please follow local GLH procedures.

Once you've sucessfully logged in, please refer to the sections on interpreting [cancer](gms_cancer_referrals.md) and / 
or [rare disease](gms_rd_referral_overview.md) referrals for further information

!!! Attention

    Users will be logged out of the system after 20 minutes of inactivity. 
    
    If a user is logged out after 20 minutes inactivity, the page will prompt the user to log back in and return to the
    page the user was last on.
    
    A user's login token will be refreshed (and therefore the 20 minute timeout will be reset) with any of the following
    actions:
    - Opening a Rare Disease case
    - Opening a Cancer case
    - Opening the tiering browser in a Rare Disease case
    - Saving a draft summary of findings
    - Clicking the "Review Cases" button
    - Downloading a file
    
    Any other network request to the API will also refresh the token and reset the timer to 20 minutes.
    
    User activity in other NGIS services e.g. DSS currently does not refresh the users access token

--8<-- "includes/abbreviations.md"
