---
hide:
  - navigation
---

??? question "What do the warnings or error messages mean?"
    
    Error or warning messages may appear in the Interpretation Portal and these are not necessarily errors within the system.

    In a particular case page, the `N/A` message, “NHS ID”, “Full Name”, “Date of Birth” and “Patient Choice” columns for this patient signify that the portal is unable to connect to TOMS at that point in time. This is expected behaviour when the data is unavailable.

    ![Image](img/faq_errors.png)
    
    - A) This message appears when the Genomics Testing Consent cannot be populated from TOMS
    
    - B) This message appears when the Interpretation Portal is unable to connect to TOMS and populate the case with PID
    
    - C) This message appears when the Interpretation Portal is unable to connect to TOMS and populate the case notes
    
    - D) This message appears when the Interpretation Portal is unable to connect to TOMS and populate the case with PID
    
    - E) This message appears when the Interpretation Portal is unable to connect to TOMS and populate the case with PID
    
    In the [Referral Grid](gms_referral_grid.md), where the cases are separated by the status tabs a message may appear 
    stating: “Unable to retrieve patient data”. 
    
    This message appears when the portal is unable to connect to TOMS and populate the case with PID.
    
??? question "What are Interpretation Services?"

    Interpretation Services are applications or algorithms that can be applied to referrals in the Genomics England 
    Interpretation Platform that facilitate users in determining the molecular basis to their patient's conditions.
    
    Each Interpretation Service applied to a referral produces an Interpreted Genome which is essentially a prioritised 
    list of variants in the cases genome identified by the algorithm.
    
    The Rare Disease [Interpretation Browser](gms_rd_tiering_browser.md) enables users to review the results of 
    Interpretation Services and select them as primary findings for the case.
    
    For further details of Tiering and Exomiser – the two current interpretation services, please refer to the 
    [Genomics England Rare Disease Genome Analysis Guide v2.0](https://future.nhs.uk/NHSgenomics/view?objectId=84281317) 
 
    Please note that currently, when the Exomiser score for a variant is 0, the score will appear to be missing in the 
    Interpretation Portal. The variant will, however, have an Exomiser ranking. 
    
    It is understood that GLHs are mandated to review variants with Exomiser ranks 1-3 and any variants with a rank 
    above 0.75. The rank for a variant can be checked, if missing from the Interpretation Portal user interface, in the 
    payload for the case. This issue where the score is missing has been confirmed to affect 100,000 Genomes Project and  
    GMS cases. This will be rectified in future to ensure that all scores, even if the score is 0, will be displayed in 
    the Interpretation Portal for variants. 
    
    At present, GLHs can check the Exomiser score for a variant in the payload of a case, downloadable via the 'Payload' 
    button on the right hand side of the case page.

??? question "How do I sign up for email notifications about my GMS referrals?"

    NHS GLHs will receive an email alerting them to any referral status updates including an alert to review cases after
    they have been sent to their respective DSS. The results email alert will be sent to the generic email address for 
    the GLH held by Genomics England. 
    If you wish to update or change the results email address, please contact the Genomics England Service desk: 
    ge-servicedesk@genomicsengland.co.uk or http://bit.ly/ge-servicedesk. 
    
    An email notification will also be sent to GLHs in the event of a SMS check failure. 

??? question "What is the Sample Matching Service?"

    The SMS provides a sample identity check. 
    
    Through SNP genotyping, SMS ensures that Rare Disease samples delivered to Genomics England match patient samples at
    the GLH, i.e. the SMS will identify any sample swaps or data inconsistencies which arise in samples delivered to 
    Genomics England.
    
    GLHs will provide NGIS with a set of SNP genotypes that will be compared by Genomics England against the WGS data.

    If a sample has passed the SMS check, a check-circle symbol will be shown by the `Sample ID` shown on the Rare 
    Disease [Referral Overview](gms_rd_referral_overview.md) page
 
    A failure of this check will be shown as a red close-circle symbol and samples which have yet to be 
    checked will be given the “pending” or “sync” symbol.
    
    ![Image](img/gms_sms_icons.png)
    
    The SMS API is queried in real time for the case – it does not store data for any cases.
    
    If you have any referrals with samples that fail the SMS check, they will move to the “Identity Check Failures” tab. 
    
    Referrals that have samples “pending” a SNP genotyping VCF upload from a GLH will be visible in the “Identity Check 
    Pending” tab. 
    
    Please contact Genomics England service desk if you have any queries over cases in the SMS tabs.
    
    Further information on the SMS can be found [here](TBC link to SMS)


??? question "Why can't I see the Genomic and Data checks on the Rare Disease Referral page?"

    The Genomics England WGS pipeline for Rare Disease performs a QC check of the reported sex against the genetic sex.

    In previous versions of the Interpretation Portal it was possible to view the outcome of this check, however, 
    because the check needed to pass for the WGS pipeline to run (and thus the referral register in the Portal) this 
    information was not informative to users so was removed

??? question "What are Decision Support Systems?"

    DSS enable users to review and report on cases in the Genomics England Interpretation Platform. They facilitate 
    MDTs and allow users to explore beyond variants highlighted by Interpretation Services. Please refer to the 
    application-specific DSS user guides for further information.

??? question "What is CVA?"

    The Clinical Variant Ark (CVA) is the Genomics England knowledge base where cases can be compared against 
    phenotypically and genotypically similar cases. For access to CVA please refer to: www.cva.genomicsengland.nhs.uk
    
??? question "What is CellBase?"

    CellBase supports annotation of many thousands of genomic features per second; it integrates biological information 
    about genomic features and proteins, gene expression regulation, functional annotation, genomic variation and 
    systems biology information.
    
    As well as CellBase providing the annotations for the tiering interpretation service, the Interpretation Browser 
    uses CellBase to provide additional variant annotations. See [Interpretation Browser](gms_rd_tiering_browser.md#additional-annotations) 
    for more info

??? question "Why is my referral flagged?"

    Please see the [Referral Flags](gms_interp_flags.md) section for more information
    

--8<-- "includes/abbreviations.md"
