“Patient Choice” (aka Consent) is taken from patients when a test is ordered in TOMS. A record of the patient choice 
decisions is visible for both Rare Disease and Cancer cases in the GMS Interpretation Portal. 


Patient Choice can be seen in two places:

1. On the [Referral grid](gms_referral_grid.md) - a column on the right-hand side called Patient Choice 
   A flag system depicting overall consent status for the referral. 
   A green flag indicates positive primary consent of all sequenced individuals to genomic testing. 
   An amber flag indicates that the consent status of least one sequenced individual is “n/a” and no sequenced individual 
   has ambiguous or negative consent. 
   A red flag indicates that at least one sequenced individual has ambiguous or negative consent. 

2. On the [Refferal Overview page](gms_rd_referral_overview.md), there is also a patient choice column beside each 
   patient in a family that shows `Yes` if patient has given consent, `No` if withdrawn consent or `N/A` if there is an 
   error or a special case of retrieving consent. 
   
Consent is usually retrieved on the fly when the case overview page is loaded. The date consent was retrieved is 
displayed in brackets next to consent value.

!!! note

    The consent value is cached and when the consent service returns a 400 or does not provide a valid response under 
    unusual circumstances, the portal displays a cached consent value - i.e. the last known value. The date next to it 
    should be indicative if it doesn't match the current date the case page is loaded.


![Image](img/gms_patient_choice.png)

???+ info "Outcome 1"

    No consent is recorded for one or more patients and the clinician has indicated that no consent form is to be sent 
    retrospectively indicating positive consent. 
    
    Referrals with no consent as an initial state do not enter the Interpretation Portal.
    
    If one member of a trio does not consent, but the other two members do consent, the referral will still be blocked from 
    the Interpretation Portal. 
    
    Patient Choice requires all patients to consent before the referral flows to the Interpretation Portal.

???+ info "Outcome 2"

    The consent discussion has happened, however instead of filling out the consent options online through the TOMs tool, 
    the clinician has filled out a paper consent form, either at the session or at a later date. The referral will be 
    blocked from entering the Interpretation Portal until the consent form has been uploaded to TOMs.
    Once the consent form(s) have been uploaded, the referral will no longer be blocked and will be visible in the 
    Interpretation Portal once the interpretation has been completed.

???+ info "Outcome 3"

    If all patients have consented to testing, and the clinician has indicated this via the TOMs consent tool, the referral
    will flow into the Interpretation Portal once interpretation has been completed.
    
    OR
    
    If a patient is unable to consent, for example due to loss of mental capacity, but they assent to the test and the 
    clinician agrees that the test is in the patient’s best interests, the referral will flow into the Interpretation Portal
    once interpretation has been completed.

???+ info "Outcome 4"

    If a patient changes their mind regarding their consent - having previously consented and now wishes to withdraw - there 
    are two possible outcomes. If the referral is already in the Interpretation Portal, the referral will stay there, and it
    will be in the hands of the clinician on whether to interpret the results or not.
    
    If the referral is not yet in the Interpretation Portal, the referral will be blocked and therefore not visible in the Portal.


## Display of Patient Choice

Patient choice in the Interpretation Portal is dynamically checked for each patient when the referral page loads.

The status for each patient will be populated on the Cancer and Rare Disease pages along with the date the patient 
choice was last checked.

!!! note 
    
    There is a small delay in the Patient Choice column updating when the case page first loads. While it is loading it
    currently displays “N/A”

In addition, there is a nightly check of the patient choice to update cases if there have been any changes added in 
TOMS

### Patient Choice display on the referral grid

There is a column on the right-hand side called “Patient Choice”. 

There is a flag system depicting overall consent status for the referral. 

A green flag indicates positive primary consent of all sequenced individuals to genomic testing (i.e. [Outcome 3](#outcome-3)). 

An amber flag indicates that the consent status of least one sequenced individual has changed since the referral was 
made and the case registered in the portal (i.e. [Outcome 4](#outcome-4)). 

A red flag indicates that at least one sequenced individual has ambiguous or negative consent.


### Cancer patient choice
![Image](img/gms_cancer_patient_choice.png)

### Rare disease patient choice
![Image](img/gms_rd_patient_choice.png)

--8<-- "includes/abbreviations.md"
