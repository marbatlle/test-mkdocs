For each referral on the case grid, the application queries the Test Order Management Service (TOMS) to retrieve Patient 
Identifiable Data (PID) and inserts it into the referral grid (Figure 1). 

If the application is unable to retrieve this data, an alert is raised in the referral grid “Unable to retrieve patient 
data”. If this message is observed the Portal is still functional, however, it will not be possible to see the PID.

See [Warning and Error Messages](TBC) for further information on error messages.

Referrals shaded in yellow in the referral grid signify that some “Draft” work for the referral has been saved.

Referrals can be flagged with tags e.g. if a family member is predicted to have Uniparental Disomy (UPD), the case 
could be tagged with “UPD”. For details of the existing case tags, please see: [Interpretation Flags](TBC) for more info


==== add abbreviation to all