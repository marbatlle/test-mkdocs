# End user documentation for the Interpretation Portal

This repo contains the end user facing documentation for the Interpretation Portal

Its hosted on gitlab pages and ends up here: https://genomicsengland.gitlab.io/interpretation/int-portal-docs

The documentation is all written in markdown, see the *.md files within the `docs` folder.

The markdown files are magically turned into pretty HTML using the mkdocs Python package and the mkdocs-material

If you need to make any changes to the documentation content... 

1. clone the repo
2. make a branch
3. make your edits of the .md files
4. git add, git commit, submit a Merge Request (MR)
5. when the MR is approved and merged into "master" branch, gitlab CI automatically pushes the updates 
6. We use [mike](https://github.com/jimporter/mike) to manage versions of the documentation. Run `mike deploy major.minor -p`
   to deploy a specific version of the documentation (see below section on Mike for more info)
   

# Using Mike   

- Mike allows multiple versions of mkdocs sites to be published and deployed

- Mike natively supports GitHub pages so needs a couple of tweaks to use GitLab pages. 

- For simplicity sake, we will use the `gh-pages` (GitHub pages) branch that mike creates to deploy our GitLab docs

- Each mkdocs site in gitlab needs a `gh-pages` branch

- A `.gitlab-ci.yml` file should be added and commited to the `gh-pages` branch

    ```
    image: alpine:3.13
    pages:
      stage: deploy
      only:
        - gh-pages
      script:
        - mkdir .public
        - cp -r * .public
        - mv .public public
        - ls
      artifacts:
        paths:
          - public
    ```
- Locally, after updates to the docs are made (as described above) run the following command on the main branch

    `mike deploy 0.1 -p`

- This makes a commit to the `gh-pages` branch and pushes that code to the remote gitlab repo. Because the `.gitlab-ci.yml`
  file is there on that branch, the pipeline is triggered which copies the contents of the `public` directory (the HTML built by mike)
  to where gitlab pages is expecting it to be. 

- Check the CI section of gitlab to confirm the pipeline has run and the new version of the docs deployed 
    

## running the docs locally

1. set up a Python virtual env and run:

    `pip install mkdocs mkdocs-material mkdocs-git-revision-date-plugin mkdocs-video mike`

2. Run:
    `mike deploy 0.1`
    `mike set-default 0.1`
    `mike serve`